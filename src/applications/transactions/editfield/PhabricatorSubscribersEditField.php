<?php

final class PhabricatorSubscribersEditField
  extends PhabricatorTokenizerEditField {

  protected function newDatasource() {
    //custom WMF change - T303829
    //return new PhabricatorMetaMTAMailableDatasource();
    return new PhabricatorPeopleDatasource();
  }

  protected function newHTTPParameterType() {
    // TODO: Implement a more expansive "Mailable" parameter type which
    // accepts users or projects.
    return new AphrontUserListHTTPParameterType();
  }

  protected function newConduitParameterType() {
    return new ConduitUserListParameterType();
  }

}
